from __future__ import print_function
from openerp.osv import fields, osv


class Inherit(osv.osv):
    _name = "inherit.parent"
    _columns = {
        'name': fields.char('Name', size=64, required=True, translate=True),
        'surname': fields.char('Surname', size=64, required=True, translate=True),
        'mname': fields.char('MiddleName', size=64, required=True, translate=True),
    }
